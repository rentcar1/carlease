package com.rentcar.carlease.lease;

import com.rentcar.carlease.core.lease.Lease;
import com.rentcar.carlease.core.lease.LeaseValidator;
import com.rentcar.carlease.core.lease.exception.InvalidLeaseException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.UUID;

import static com.rentcar.carlease.core.lease.LeaseConstants.*;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class LeaseValidatorTest {

    private LeaseValidator leaseValidator;

    @BeforeEach
    public void beforeEach() {
        leaseValidator = new LeaseValidator();
    }

    @Test
    @DisplayName("Given a valid leases should't throws any Exception")
    public void validate_whenValidLease() {

        // given
        var lease = new Lease(UUID.randomUUID().toString(),
                UUID.randomUUID().toString(),
                UUID.randomUUID().toString(),
                java.sql.Date.valueOf(LocalDate.now()),
                java.sql.Date.valueOf(LocalDate.now().plusDays(30)),
                null);

        // when
        leaseValidator.validate(lease);

        // then no exception is fired.

    }

    @Test
    @DisplayName("Given a lease with null car Id should throws Exception")
    public void validate_nullCarId_throwsException() {

        // given
        var lease = new Lease(null,
                null,
                UUID.randomUUID().toString(),
                java.sql.Date.valueOf(LocalDate.now()),
                java.sql.Date.valueOf(LocalDate.now().plusDays(30)),
                null);

        // when
        Exception exception = assertThrows(InvalidLeaseException.class, () -> {
            leaseValidator.validate(lease);
        });

        // then
        assertTrue(exception.getMessage().contains(INVALID_LEASE_MSG));
    }

    @Test
    @DisplayName("Given a lease with null driver Id should throws Exception")
    public void validate_nullDriverId_throwsException() {

        // given
        var lease = new Lease(null,
                UUID.randomUUID().toString(),
                null,
                java.sql.Date.valueOf(LocalDate.now()),
                java.sql.Date.valueOf(LocalDate.now().plusDays(30)),
                null);

        // when
        Exception exception = assertThrows(InvalidLeaseException.class, () -> {
            leaseValidator.validate(lease);
        });

        // then
        assertTrue(exception.getMessage().contains(INVALID_LEASE_MSG));
    }

    @Test
    @DisplayName("Given a lease with null startDate should throws Exception")
    public void validate_nullStartDate_throwsException() {

        // given
        var lease = new Lease(null,
                UUID.randomUUID().toString(),
                UUID.randomUUID().toString(),
                null,
                java.sql.Date.valueOf(LocalDate.now().plusDays(30)),
                null);

        // when
        Exception exception = assertThrows(InvalidLeaseException.class, () -> {
            leaseValidator.validate(lease);
        });

        // then
        assertTrue(exception.getMessage().contains(INVALID_LEASE_MSG));
    }

    @Test
    @DisplayName("Given a lease with end date before start date should throws Exception")
    public void validate_endDateBeforeStartDate_throwsException() {

        // given
        var localStartDate = LocalDate.now();
        var startDate = java.sql.Date.valueOf(localStartDate);
        var endDate = java.sql.Date.valueOf(localStartDate.minus(30, ChronoUnit.DAYS));
        var lease = new Lease(UUID.randomUUID().toString(),
                UUID.randomUUID().toString(),
                UUID.randomUUID().toString(),
                startDate,
                endDate,
                null);

        // when
        Exception exception = assertThrows(InvalidLeaseException.class, () -> {
            leaseValidator.validate(lease);
        });

        // then
        assertTrue(exception.getMessage().contains(INVALID_PERIOD_DATES_MSG));
    }

    @Test
    @DisplayName("Given a lease with start date after end date should throws Exception")
    public void validate_starDateAfterEndDate_throwsException() {

        // given
        var localEndDate = LocalDate.now();
        var endDate = java.sql.Date.valueOf(localEndDate.now());
        var startDate = java.sql.Date.valueOf(localEndDate.plusDays(30));

        var lease = new Lease(UUID.randomUUID().toString(),
                UUID.randomUUID().toString(),
                UUID.randomUUID().toString(),
                startDate,
                endDate,
                null);

        // when
        Exception exception = assertThrows(InvalidLeaseException.class, () -> {
            leaseValidator.validate(lease);
        });

        // then
        assertTrue(exception.getMessage().contains(INVALID_PERIOD_DATES_MSG));
    }

    @Test
    @DisplayName("Given a lease with finish date before start date should throws Exception")
    public void validate_finishDateBeforeStartDate_throwsException() {

        // given
        var localStartDate = LocalDate.now();
        var startDate = java.sql.Date.valueOf(localStartDate);
        var endDate = java.sql.Date.valueOf(localStartDate.plusDays(30));
        var finishDate = java.sql.Date.valueOf(localStartDate.minus(30, ChronoUnit.DAYS));
        var lease = new Lease(UUID.randomUUID().toString(),
                UUID.randomUUID().toString(),
                UUID.randomUUID().toString(),
                startDate,
                endDate,
                finishDate);

        // when
        Exception exception = assertThrows(InvalidLeaseException.class, () -> {
            leaseValidator.validate(lease);
        });

        // then
        assertTrue(exception.getMessage().contains(INVALID_FINISHED_DATE_MSG));
    }

}
