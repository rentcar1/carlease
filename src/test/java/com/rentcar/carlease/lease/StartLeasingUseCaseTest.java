package com.rentcar.carlease.lease;

import com.rentcar.carlease.core.car.Car;
import com.rentcar.carlease.core.car.CarRepository;
import com.rentcar.carlease.core.driver.Driver;
import com.rentcar.carlease.core.driver.exception.DriverNorFoundException;
import com.rentcar.carlease.core.driver.DriverService;
import com.rentcar.carlease.core.lease.Lease;
import com.rentcar.carlease.core.lease.LeaseRepository;
import com.rentcar.carlease.core.lease.StartLeasingUseCase;
import com.rentcar.carlease.core.lease.LeaseValidator;
import com.rentcar.carlease.core.lease.exception.CarLeaseAlreadyRentedException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

public class StartLeasingUseCaseTest {

    @Mock
    private LeaseRepository leaseRepository;

    @Mock
    private CarRepository carRepository;

    @Mock
    private DriverService driverService;

    @Mock
    private LeaseValidator leaseValidator;

    private StartLeasingUseCase startLeasingUseCase;

    @BeforeEach
    public void beforeEach() {
        MockitoAnnotations.openMocks(this);
        startLeasingUseCase = new StartLeasingUseCase(
                this.leaseRepository,
                this.driverService,
                this.leaseValidator,
                this.carRepository);
    }

    @Test
    @DisplayName("Given a lease with a valid car and driver should start a leasing")
    public void startLeasing_validLease_startLeasing() {

        // Given
        var carId = UUID.randomUUID().toString();
        var driverId = UUID.randomUUID().toString();
        var lease = new Lease(UUID.randomUUID().toString(), carId, driverId,
                java.sql.Date.valueOf(LocalDate.now()),
                java.sql.Date.valueOf(LocalDate.now().plusDays(30)),
                null);

        var driver = new Driver(driverId, "0897635252");
        UUID udriverId = UUID.fromString(driverId);
        when(driverService.getDriverById(udriverId)).thenReturn(Optional.of(driver));
        when(carRepository.findById(carId)).thenReturn(Optional.of(new Car(carId,"AXD8383")));
        when(leaseRepository.findByDriverId(driverId)).thenReturn(new ArrayList<>());

        // when
        startLeasingUseCase.startLeasing(lease);

        // then
        verify(leaseRepository, times(1)).saveLease(lease);
        verify(leaseValidator, times(1)).validate(lease);
    }

    @Test
    @DisplayName("Given a lease with a car already rented should Throws CarAlreadyRentedException")
    public void startLeasing_carAlreadyRented_throwsException() {

        // Given
        var carId = UUID.randomUUID().toString();
        var driverId = UUID.randomUUID().toString();
        var lease = new Lease(UUID.randomUUID().toString(), carId, driverId,
                java.sql.Date.valueOf(LocalDate.now()),
                java.sql.Date.valueOf(LocalDate.now().plusDays(30)),
                null);
        var existentLease = new Lease(UUID.randomUUID().toString(), carId, UUID.randomUUID().toString(),
                java.sql.Date.valueOf(LocalDate.now()),
                java.sql.Date.valueOf(LocalDate.now().plusDays(30)),
                null);

        var driver = new Driver(driverId, "0897635252");
        UUID udriverId = UUID.fromString(driverId);
        when(driverService.getDriverById(udriverId)).thenReturn(Optional.of(driver));
        when(carRepository.findById(carId)).thenReturn(Optional.of(new Car(carId,"AXD8383")));

        List<Lease> leaseList = new ArrayList<Lease>();
        leaseList.add(existentLease);
        when(leaseRepository.findByCarId(carId)).thenReturn(leaseList);

        // then
        Exception exception = assertThrows(CarLeaseAlreadyRentedException.class, () -> {
            // when
            startLeasingUseCase.startLeasing(lease);
        });

        verify(leaseRepository, times(0)).saveLease(lease);
    }

    @Test
    @DisplayName("Given a lease with a non existent driver should Throws DriverNotFoundException")
    public void startLeasing_driverNotFound_throwsException() {

        // Given
        var carId = UUID.randomUUID().toString();
        var driverId = UUID.randomUUID().toString();
        var lease = new Lease(UUID.randomUUID().toString(), carId, driverId,
                java.sql.Date.valueOf(LocalDate.now()),
                java.sql.Date.valueOf(LocalDate.now().plusDays(30)),
                null);

        when(carRepository.findById(carId)).thenReturn(Optional.of(new Car(carId,"AXD8383")));
        UUID udriverId = UUID.fromString(driverId);
        when(driverService.getDriverById(udriverId)).thenReturn(Optional.empty());

        // than
        Exception exception = assertThrows(DriverNorFoundException.class, () -> {
            // when
            startLeasingUseCase.startLeasing(lease);
        });

        verify(leaseRepository, times(0)).saveLease(lease);
    }



}
