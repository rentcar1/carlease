package com.rentcar.carlease.lease;

import com.rentcar.carlease.core.lease.*;
import com.rentcar.carlease.core.lease.exception.LeaseNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.time.LocalDate;
import java.util.Optional;
import java.util.UUID;

import static com.rentcar.carlease.core.lease.LeaseConstants.LEASE_NOT_FOUND_MSG;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

public class FinishLeasingUseCaseTest {

    @Mock
    private LeaseRepository leaseRepository;

    private FinishLeasingUseCase finishLeasingUseCase;

    @BeforeEach
    public void beforeEach() {
        MockitoAnnotations.openMocks(this);
        finishLeasingUseCase = new FinishLeasingUseCase(
                this.leaseRepository);
    }

    @Test
    @DisplayName("Given a lease with a non existent id should Throws DriverNotFoundException")
    public void finishLeasing_driverNotFound_throwsException() {

        // Given
        var lease = new Lease(
                UUID.randomUUID().toString(),
                UUID.randomUUID().toString(),
                UUID.randomUUID().toString(),
                java.sql.Date.valueOf(LocalDate.now()),
                java.sql.Date.valueOf(LocalDate.now().plusDays(30)),
                null);

        when(leaseRepository.findById(lease.id)).thenReturn(Optional.empty());

        // than
        Exception exception = assertThrows(LeaseNotFoundException.class, () -> {
            // when
            finishLeasingUseCase.finishLeasing(lease.id);
        });

        verify(leaseRepository, times(0)).finishLease(lease);
        assertTrue(exception.getMessage().contains(LEASE_NOT_FOUND_MSG));
    }
}
