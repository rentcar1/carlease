package com.rentcar.carlease.framework.spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan(value = {"com.rentcar.carlease"})
@SpringBootApplication
@EnableFeignClients
public class CarleaseApplication {

	public static void main(String[] args) {
		SpringApplication.run(CarleaseApplication.class, args);
	}


}
