package com.rentcar.carlease.framework.spring.driver.service;

import java.io.Serializable;
import java.util.UUID;

/**
 * Web DTO
 */
public class DriverWeb implements Serializable {

    public final UUID id;

    public final String cpf;

    public DriverWeb() {
        this.cpf = "";
        this.id = null;
    }

    public DriverWeb(UUID id, String cpf) {
        this.id = id;
        this.cpf = cpf;
    }


}
