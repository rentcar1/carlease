package com.rentcar.carlease.framework.spring.lease.repository;

import org.springframework.data.annotation.Id;

import java.util.Date;

public class LeaseEntity {

    @Id
    public final String id;

    public final String carId;

    public final String driverId;

    public final Date startDate;

    public final Date endDate;

    public final Date finishDate;

    public LeaseEntity(String id, String carId, String driverId, Date startDate, Date endDate, Date finishDate) {
        this.id = id;
        this.carId = carId;
        this.driverId = driverId;
        this.startDate = startDate;
        this.endDate = endDate;
        this.finishDate = finishDate;
    }
}
