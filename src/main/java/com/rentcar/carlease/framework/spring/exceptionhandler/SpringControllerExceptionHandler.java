package com.rentcar.carlease.framework.spring.exceptionhandler;

import com.rentcar.carlease.core.car.exception.CarAlreadyExistsException;
import com.rentcar.carlease.core.car.exception.CarNotFoundException;
import com.rentcar.carlease.core.car.exception.CarValidationException;
import com.rentcar.carlease.core.lease.exception.CarLeaseAlreadyRentedException;
import com.rentcar.carlease.core.lease.exception.CarLeaseNotFoundException;
import com.rentcar.carlease.core.lease.exception.InvalidLeaseException;
import com.rentcar.carlease.core.lease.exception.LeaseNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.LocalDateTime;

@ControllerAdvice
public class SpringControllerExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(
            {
                    CarAlreadyExistsException.class,
                    CarNotFoundException.class,
                    CarValidationException.class,
                    CarLeaseAlreadyRentedException.class,
                    CarLeaseNotFoundException.class,
                    InvalidLeaseException.class,
                    LeaseNotFoundException.class
            })
    public ResponseEntity<ErrorResponse> handleError(Exception ex, WebRequest request) {

        ErrorResponse errorResponse = new ErrorResponse();
        errorResponse.setError(ex.getMessage());
        errorResponse.setTimestamp(LocalDateTime.now());
        errorResponse.setStatus(HttpStatus.BAD_REQUEST.value());

        return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
    }

}
