package com.rentcar.carlease.framework.spring.lease.controller;

import com.rentcar.carlease.adapter.lease.controller.FinishLeasingWeb;
import com.rentcar.carlease.adapter.lease.controller.LeaseController;
import com.rentcar.carlease.adapter.lease.controller.LeaseWeb;
import com.rentcar.carlease.adapter.lease.controller.StartLeaseWeb;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("leasing")
public class SpringLeaseController {

    private final LeaseController leaseController;

    @Autowired
    public SpringLeaseController(LeaseController leaseController) {
        this.leaseController = leaseController;
    }

    @PostMapping("/start")
    public LeaseWeb startLeasing(@RequestBody StartLeaseWeb startLeaseWeb) {

        return leaseController.startLeasing(startLeaseWeb);
    }

    @PostMapping("/finish")
    public void finishLeasing(@RequestBody FinishLeasingWeb finishLeasingWeb) {
        leaseController.finishLeasing(finishLeasingWeb);
    }

}
