package com.rentcar.carlease.framework.spring.car.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface CarMongoRepository extends MongoRepository<CarEntity, String> {

    Optional<CarEntity> findByLicense(String license);


}
