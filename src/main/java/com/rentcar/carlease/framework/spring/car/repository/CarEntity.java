package com.rentcar.carlease.framework.spring.car.repository;

import org.springframework.data.annotation.Id;

public class CarEntity {

    @Id
    public final String id;

    public final String license;

    public CarEntity(String id, String license) {
        this.id = id;
        this.license = license;
    }
}
