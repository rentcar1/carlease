package com.rentcar.carlease.framework.spring.lease.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LeaseMongoRepository extends MongoRepository<LeaseEntity, String> {

    List<LeaseEntity> findByDriverId(String driverId);

    List<LeaseEntity> findByCarId(String driverId);

}
