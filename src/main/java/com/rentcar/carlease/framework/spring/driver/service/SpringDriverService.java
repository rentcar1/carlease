package com.rentcar.carlease.framework.spring.driver.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;

import java.util.UUID;

@FeignClient("driver-api")
public interface SpringDriverService {

    @GetMapping("/{id}")
    DriverWeb getDriver(@RequestHeader(value = "Authorization", required = true) String authHeader,
                        @PathVariable("id") UUID id);

    @PostMapping("/login")
    String login(User user);

}
