package com.rentcar.carlease.framework.spring.car.controller;

import com.rentcar.carlease.adapter.car.controller.CarController;
import com.rentcar.carlease.adapter.car.controller.CarWeb;
import com.rentcar.carlease.adapter.car.controller.CreateCarWeb;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SpringCarController {

    private final CarController carController;

    @Autowired
    public SpringCarController(CarController carController) {
        this.carController = carController;
    }

    @PostMapping("/save")
    public CarWeb createCar(@RequestBody final CreateCarWeb createCarWeb ) {
        return carController.createCar(createCarWeb);
    }

    @GetMapping("/test")
    public String test() {
        return "OK";
    }

}
