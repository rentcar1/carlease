package com.rentcar.carlease.framework.spring.car.config;

import com.rentcar.carlease.adapter.car.config.CarSpringConfig;
import com.rentcar.carlease.adapter.car.controller.CarController;
import com.rentcar.carlease.adapter.car.repository.CarRepositoryImpl;
import com.rentcar.carlease.framework.spring.car.repository.CarMongoRepository;
import com.rentcar.carlease.core.car.CarRepository;
import com.rentcar.carlease.core.car.CreateCarUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Connects the Core to the Spring Application by
 * using adapter layer throw SpringConfig.
 */
@Configuration
public class CarConfig {

    private CarRepository carRepository;

    private CarMongoRepository carMongoRepository;

    private CarSpringConfig carSpringConfig;

    @Autowired
    public CarConfig(CarMongoRepository carMongoRepository) {
        this.carMongoRepository = carMongoRepository;
        this.carRepository = new CarRepositoryImpl(carMongoRepository);
        this.carSpringConfig = new CarSpringConfig(carRepository);
    }

    @Bean
    public CreateCarUseCase createCar() {
        return carSpringConfig.createCar();
    }

    @Bean
    public CarController carController() {
        return new CarController(createCar());
    }

}
