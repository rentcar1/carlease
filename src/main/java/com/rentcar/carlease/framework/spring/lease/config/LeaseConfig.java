package com.rentcar.carlease.framework.spring.lease.config;

import com.rentcar.carlease.adapter.car.repository.CarRepositoryImpl;
import com.rentcar.carlease.adapter.driver.service.DriverServiceImpl;
import com.rentcar.carlease.adapter.lease.config.LeaseSpringConfig;
import com.rentcar.carlease.adapter.lease.controller.LeaseController;
import com.rentcar.carlease.adapter.lease.repository.LeaseRepositoryImpl;
import com.rentcar.carlease.core.car.CarRepository;
import com.rentcar.carlease.framework.spring.car.repository.CarMongoRepository;
import com.rentcar.carlease.framework.spring.driver.service.SpringDriverService;
import com.rentcar.carlease.framework.spring.lease.repository.LeaseMongoRepository;
import com.rentcar.carlease.core.driver.DriverService;
import com.rentcar.carlease.core.lease.FinishLeasingUseCase;
import com.rentcar.carlease.core.lease.LeaseRepository;
import com.rentcar.carlease.core.lease.StartLeasingUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Connects the Core to the Spring Framework by
 * using adapter layer throw SpringConfig.
 */
@Configuration
public class LeaseConfig {

    private LeaseMongoRepository leaseMongoRepository;

    private CarMongoRepository carMongoRepository;

    private SpringDriverService springDriverService;

    private LeaseRepository leaseRepository;

    private CarRepository carRepository;

    private DriverService driverService;

    private LeaseSpringConfig leaseSpringConfig;

    @Autowired
    public LeaseConfig(LeaseMongoRepository leaseMongoRepository,
                       CarMongoRepository carMongoRepository,
                       SpringDriverService springDriverService) {

        this.leaseMongoRepository = leaseMongoRepository;
        this.carMongoRepository = carMongoRepository;
        this.springDriverService = springDriverService;

        this.leaseRepository = new LeaseRepositoryImpl(leaseMongoRepository);
        this.carRepository = new CarRepositoryImpl(carMongoRepository);
        this.driverService = new DriverServiceImpl(springDriverService);
        this.leaseSpringConfig = new LeaseSpringConfig(leaseRepository, driverService, carRepository);
    }

    @Bean
    public StartLeasingUseCase startLeasing() {
        return leaseSpringConfig.startLeasing();
    }

    @Bean
    public FinishLeasingUseCase finishLeasingUseCase() {
        return leaseSpringConfig.finishLeasing();
    }

    @Bean
    public LeaseController leaserController() {
        return new LeaseController(startLeasing(), finishLeasingUseCase());
    }


}
