package com.rentcar.carlease.adapter.lease.controller;

import com.rentcar.carlease.core.lease.FinishLeasingUseCase;
import com.rentcar.carlease.core.lease.StartLeasingUseCase;

public class LeaseController {

    private final StartLeasingUseCase startLeasingUseCase;

    private final FinishLeasingUseCase finishLeasingUseCase;

    public LeaseController(StartLeasingUseCase startLeasingUseCase, FinishLeasingUseCase finishLeasingUseCase) {
        this.startLeasingUseCase = startLeasingUseCase;
        this.finishLeasingUseCase = finishLeasingUseCase;
    }

    public LeaseWeb startLeasing(final StartLeaseWeb startLeaseWeb) {
        var lease = startLeaseWeb.toLease(startLeaseWeb);
        return LeaseWeb.toLeaseWeb(this.startLeasingUseCase.startLeasing(lease));
    }

    public void finishLeasing(final FinishLeasingWeb finishLeasingWeb) {
        this.finishLeasingUseCase.finishLeasing(finishLeasingWeb.id);
    }

}
