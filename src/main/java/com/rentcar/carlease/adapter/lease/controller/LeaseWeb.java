package com.rentcar.carlease.adapter.lease.controller;

import com.rentcar.carlease.core.lease.Lease;

import java.io.Serializable;
import java.util.Date;

public class LeaseWeb implements Serializable {

    public final String id;

    public final String carId;

    public final String driverId;

    public final Date startDate;

    public final Date endDate;

    public final Date finishDate;

    public LeaseWeb(String id, String carId, String driverId, Date startDate, Date endDate, Date finishDate) {
        this.id = id;
        this.carId = carId;
        this.driverId = driverId;
        this.startDate = startDate;
        this.endDate = endDate;
        this.finishDate = finishDate;
    }

    public LeaseWeb() {
        this.id = null;
        this.carId = null;
        this.driverId = null;
        this.startDate = null;
        this.endDate = null;
        this.finishDate = null;
    }

    public static class Builder {
        private String id;
        private String carId;
        private String driverId;
        private Date startDate;
        private Date endDate;
        private Date finishDate;

        public LeaseWeb.Builder id(String id) { this.id = id; return this; }
        public LeaseWeb.Builder carId(String carId) { this.carId = carId; return this; }
        public LeaseWeb.Builder driverId(String driverId) { this.driverId = driverId; return this; }
        public LeaseWeb.Builder startDate(Date startDate) { this.startDate = startDate; return this; }
        public LeaseWeb.Builder endDate(Date endDate) { this.endDate = endDate; return this; }
        public LeaseWeb.Builder finishDate(Date finishDate) { this.finishDate = finishDate; return this; }

        public LeaseWeb build(){
            return new LeaseWeb(id, carId, driverId,startDate,endDate,finishDate);
        }
    }

    public Lease toLease(LeaseWeb leaseWeb) {
        return new Lease.Builder()
                .id(id)
                .carId(carId)
                .driverId(driverId)
                .startDate(startDate)
                .endDate(endDate)
                .finishDate(finishDate)
                .build();
    }

    public static LeaseWeb toLeaseWeb(Lease lease) {
        return new LeaseWeb.Builder()
                .id(lease.id)
                .carId(lease.carId)
                .driverId(lease.driverId)
                .startDate(lease.startDate)
                .endDate(lease.endDate)
                .finishDate(lease.finishDate)
                .build();
    }
}
