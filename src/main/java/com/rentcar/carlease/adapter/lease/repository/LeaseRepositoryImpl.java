package com.rentcar.carlease.adapter.lease.repository;

import com.rentcar.carlease.core.lease.Lease;
import com.rentcar.carlease.core.lease.LeaseRepository;
import com.rentcar.carlease.framework.spring.lease.repository.LeaseEntity;
import com.rentcar.carlease.framework.spring.lease.repository.LeaseMongoRepository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class LeaseRepositoryImpl implements LeaseRepository {

    private LeaseMongoRepository repository;

    public LeaseRepositoryImpl(LeaseMongoRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Lease> findByDriverId(String driverId) {
        return repository.findByDriverId(driverId)
                .stream()
                .map( l -> {
                    var lease = new Lease(l.id, l.carId, l.driverId, l.startDate, l.endDate, l.finishDate);
                    return lease;
                })
                .collect(Collectors.toList());
    }

    @Override
    public List<Lease> findByCarId(String carId) {
        return repository.findByCarId(carId)
                .stream()
                .map( l -> {
                    var lease = new Lease(l.id, l.carId, l.driverId, l.startDate, l.endDate, l.finishDate);
                    return lease;
                })
                .collect(Collectors.toList());
    }

    @Override
    public Optional<Lease> findById(String leaseId) {
        var leaseEntity = repository.findById(leaseId);
        if (leaseEntity.isEmpty()) {
            return Optional.empty();
        }
        return Optional.of(
                new Lease(
                        leaseEntity.get().id,
                        leaseEntity.get().carId,
                        leaseEntity.get().driverId,
                        leaseEntity.get().startDate,
                        leaseEntity.get().endDate,
                        leaseEntity.get().finishDate)
        );
    }

    @Override
    public Lease saveLease(Lease lease) {
        var leaseEntity = new LeaseEntity(
                lease.id,
                lease.carId,
                lease.driverId,
                lease.startDate,
                lease.endDate,
                lease.finishDate);

        leaseEntity = repository.save(leaseEntity);

        return new Lease(leaseEntity.id,
                leaseEntity.driverId,
                leaseEntity.carId,
                leaseEntity.startDate,
                leaseEntity.endDate,
                leaseEntity.finishDate);
    }

    @Override
    public void finishLease(Lease lease) {
        var leaseEntity = new LeaseEntity(
                lease.id,
                lease.carId,
                lease.driverId,
                lease.startDate,
                lease.endDate,
                lease.finishDate);

        repository.save(leaseEntity);
    }
}
