package com.rentcar.carlease.adapter.lease.controller;

import java.io.Serializable;

public class FinishLeasingWeb implements Serializable {

    public final String id;

    public FinishLeasingWeb(String id) {
        this.id = id;
    }

    public FinishLeasingWeb() {
        this.id = "";
    }
}
