package com.rentcar.carlease.adapter.lease.controller;

import com.rentcar.carlease.core.lease.Lease;

import java.io.Serializable;
import java.util.Date;

public class StartLeaseWeb implements Serializable {

    public final String carId;

    public final String driverId;

    public final Date startDate;

    public final Date endDate;

    public StartLeaseWeb(String carId, String driverId, Date startDate, Date endDate) {
        this.carId = carId;
        this.driverId = driverId;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public StartLeaseWeb() {
        this.carId = null;
        this.driverId = null;
        this.startDate = null;
        this.endDate = null;
    }

    public static class Builder {
        private String carId;
        private String driverId;
        private Date startDate;
        private Date endDate;

        public StartLeaseWeb.Builder carId(String carId) { this.carId = carId; return this; }
        public StartLeaseWeb.Builder driverId(String driverId) { this.driverId = driverId; return this; }
        public StartLeaseWeb.Builder startDate(Date startDate) { this.startDate = startDate; return this; }
        public StartLeaseWeb.Builder endDate(Date endDate) { this.endDate = endDate; return this; }

        public StartLeaseWeb build(){
            return new StartLeaseWeb(carId, driverId,startDate, endDate);
        }
    }

    public Lease toLease(StartLeaseWeb startLeaseWeb) {
        return new Lease.Builder()
                .id(null)
                .driverId(startLeaseWeb.driverId)
                .carId(startLeaseWeb.carId)
                .startDate(startLeaseWeb.startDate)
                .endDate(startLeaseWeb.endDate)
                .finishDate(null)
                .build();
    }


}
