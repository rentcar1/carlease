package com.rentcar.carlease.adapter.lease.config;


import com.rentcar.carlease.core.car.CarRepository;
import com.rentcar.carlease.core.driver.DriverService;
import com.rentcar.carlease.core.lease.FinishLeasingUseCase;
import com.rentcar.carlease.core.lease.LeaseRepository;
import com.rentcar.carlease.core.lease.LeaseValidator;
import com.rentcar.carlease.core.lease.StartLeasingUseCase;

/**
 * Lease specific configuration for Spring Framework.
 */
public class LeaseSpringConfig {

    private LeaseRepository leaseRepository;

    private CarRepository carRepository;

    private DriverService driverService;

    public LeaseSpringConfig(
            LeaseRepository leaseRepository,
            DriverService driverService,
            CarRepository carRepository)
    {
        this.leaseRepository = leaseRepository ;
        this.driverService = driverService;
        this.carRepository = carRepository;
    }

    public StartLeasingUseCase startLeasing(){
        return new StartLeasingUseCase(
                leaseRepository,
                driverService,
                new LeaseValidator(),
                carRepository);
    }

    public FinishLeasingUseCase finishLeasing(){
        return new FinishLeasingUseCase(leaseRepository);
    }

}
