package com.rentcar.carlease.adapter.car.config;


import com.rentcar.carlease.core.car.CarRepository;
import com.rentcar.carlease.core.car.CarValidator;
import com.rentcar.carlease.core.car.CreateCarUseCase;

/**
 * Specific configuration for Spring Framework.
 */
public class CarSpringConfig {

    private CarRepository carRepository;

    public CarSpringConfig(CarRepository carRepository)
    {
        this.carRepository = carRepository;
    }

    public CreateCarUseCase createCar(){
        return new CreateCarUseCase(carRepository, new CarValidator());
    }

}
