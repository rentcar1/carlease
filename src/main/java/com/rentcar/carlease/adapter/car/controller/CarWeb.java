package com.rentcar.carlease.adapter.car.controller;

import com.rentcar.carlease.core.car.Car;

import java.io.Serializable;

public class CarWeb implements Serializable {

    public final String id;

    public final String license;

    public CarWeb() {
        this.id = null;
        this.license = null;
    }

    public CarWeb(String id, String license) {
        this.id = id;
        this.license = license;
    }

    public static class Builder {
        private String id;
        private String license;

        public Builder id(String id) {
            this.id = id;
            return this;
        }

        public Builder license(String license) {
            this.license = license;
            return this;
        }

        public CarWeb build(){
            return new CarWeb(id, license);
        }
    }

    public Car toCar(CarWeb carWeb) {
        return new Car.Builder()
                .id(id)
                .license(license)
                .build();
    }

    public static CarWeb toCarWeb(Car car) {
        return new CarWeb.Builder()
                .id(car.id)
                .license(car.license)
                .build();
    }




}
