package com.rentcar.carlease.adapter.car.controller;

import com.rentcar.carlease.core.car.Car;

import java.io.Serializable;

public class CreateCarWeb implements Serializable {

    public final String license;

    public CreateCarWeb() {
        this.license = null;
    }

    public CreateCarWeb(String license) {
        this.license = license;
    }

    public static class Builder {
        private String license;

        public CreateCarWeb.Builder license(String license) {
            this.license = license;
            return this;
        }

        public CreateCarWeb build(){
            return new CreateCarWeb(license);
        }
    }

    public Car toCar(CreateCarWeb carWeb) {
        return new Car.Builder()
                .id(null)
                .license(license)
                .build();
    }

    public static CarWeb toCarWeb(Car car) {
        return new CarWeb.Builder()
                .id(car.id)
                .license(car.license)
                .build();
    }

}
