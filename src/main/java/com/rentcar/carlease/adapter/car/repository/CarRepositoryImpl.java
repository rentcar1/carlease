package com.rentcar.carlease.adapter.car.repository;

import com.rentcar.carlease.core.car.Car;
import com.rentcar.carlease.core.car.CarRepository;
import com.rentcar.carlease.framework.spring.car.repository.CarEntity;
import com.rentcar.carlease.framework.spring.car.repository.CarMongoRepository;

import java.util.Optional;

public class CarRepositoryImpl implements CarRepository {

    private CarMongoRepository carMongoRepository;

    public CarRepositoryImpl(CarMongoRepository carMongoRepository) {
        this.carMongoRepository = carMongoRepository;
    }

    @Override
    public Car create(Car car) {
        var carEntity = new CarEntity(null, car.license);
        carEntity = carMongoRepository.save(carEntity);
        return new Car(carEntity.id, carEntity.license);
    }

    @Override
    public Optional<Car> findByLicense(String license) {
        var carEntity = carMongoRepository.findByLicense(license);
        if (carEntity.isEmpty()) {
            return Optional.empty();
        }
        return Optional.of(new Car(carEntity.get().id, carEntity.get().license)) ;
    }

    @Override
    public Optional<Car> findById(String id) {
        var carEntity = carMongoRepository.findById(id);
        if (carEntity.isEmpty()) {
            return Optional.empty();
        }
        return Optional.of(new Car(carEntity.get().id, carEntity.get().license)) ;
    }

}
