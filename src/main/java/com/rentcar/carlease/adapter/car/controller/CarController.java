package com.rentcar.carlease.adapter.car.controller;

import com.rentcar.carlease.core.car.CreateCarUseCase;

public class CarController {

    private final CreateCarUseCase carUseCase;

    public CarController(CreateCarUseCase carUseCase) {
        this.carUseCase = carUseCase;
    }

    public CarWeb createCar(final CreateCarWeb createCarWeb) {
        var car = createCarWeb.toCar(createCarWeb);
        return CarWeb.toCarWeb(this.carUseCase.createCar(car));
    }

}
