package com.rentcar.carlease.adapter.driver.service;

import com.rentcar.carlease.framework.spring.driver.service.SpringDriverService;
import com.rentcar.carlease.core.driver.Driver;
import com.rentcar.carlease.core.driver.DriverService;
import com.rentcar.carlease.framework.spring.driver.service.User;

import java.util.Optional;
import java.util.UUID;

public class DriverServiceImpl implements DriverService {

    private SpringDriverService springDriverService;

    public DriverServiceImpl(SpringDriverService springDriverService) {
        this.springDriverService = springDriverService;
    }

    @Override
    public Optional<Driver> getDriverById(UUID driverId) {

        // get access token
        final var user = new User("test","test");
        final String token = springDriverService.login(user);
        final String authHeader = token.replace("test", "Bearer");

        var driverWeb = springDriverService.getDriver(authHeader, driverId);

        var driver = new Driver(driverWeb.id.toString(), driverWeb.cpf);

        return Optional.of(driver);
    }
}
