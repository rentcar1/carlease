package com.rentcar.carlease.core.lease.exception;

public class LeaseNotFoundException extends RuntimeException{
    public LeaseNotFoundException(String message) {
        super(message);
    }
}
