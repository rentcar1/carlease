package com.rentcar.carlease.core.lease.exception;

public class CarLeaseAlreadyRentedException extends RuntimeException{
    public CarLeaseAlreadyRentedException(String message) {
        super(message);
    }
}
