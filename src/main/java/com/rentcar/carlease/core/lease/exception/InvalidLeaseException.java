package com.rentcar.carlease.core.lease.exception;

public class InvalidLeaseException extends RuntimeException{
    public InvalidLeaseException(String message) {
        super(message);
    }
}
