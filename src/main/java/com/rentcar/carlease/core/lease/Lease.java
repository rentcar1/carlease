package com.rentcar.carlease.core.lease;

import javax.validation.constraints.NotNull;
import java.util.Date;

public class  Lease {

    public final String id;

    @NotNull(message = "Car Id cannot be null")
    public final String carId;

    @NotNull(message = "Driver Id cannot be null")
    public final String driverId;

    /** The start date for the lease contract */
    @NotNull(message = "Start date cannot be null")
    public final Date startDate;

    /** The end date for the lease contract */
    @NotNull(message = "End date cannot be null")
    public final Date endDate;

    /** Finished contract date */
    public final Date finishDate;

    public Lease(String id, String carId, String driverId, Date startDate, Date endDate, Date finishDate) {
        this.id = id;
        this.carId = carId;
        this.driverId = driverId;
        this.startDate = startDate;
        this.endDate = endDate;
        this.finishDate = finishDate;
    }

    public static class Builder {
        private String id;
        private String carId;
        private String driverId;
        private Date startDate;
        private Date endDate;
        private Date finishDate;

        public Lease.Builder id(String id) { this.id = id; return this; }
        public Lease.Builder carId(String carId) { this.carId = carId; return this; }
        public Lease.Builder driverId(String driverId) { this.driverId = driverId; return this; }
        public Lease.Builder startDate(Date startDate) { this.startDate = startDate; return this; }
        public Lease.Builder endDate(Date endDate) { this.endDate = endDate; return this; }
        public Lease.Builder finishDate(Date finishDate) { this.finishDate = finishDate; return this; }

        public Lease build(){
            return new Lease(id, carId, driverId,startDate,endDate,finishDate);
        }
    }
}

