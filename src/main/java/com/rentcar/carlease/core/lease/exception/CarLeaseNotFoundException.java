package com.rentcar.carlease.core.lease.exception;

public class CarLeaseNotFoundException extends RuntimeException{
    public CarLeaseNotFoundException(String message) {
        super(message);
    }
}
