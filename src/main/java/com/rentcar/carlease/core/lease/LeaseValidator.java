package com.rentcar.carlease.core.lease;

import com.rentcar.carlease.core.lease.exception.InvalidLeaseException;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import static com.rentcar.carlease.core.lease.LeaseConstants.*;

public class LeaseValidator {

    private static final Logger LOGGER = Logger.getLogger(LeaseValidator.class.getName());

    public void validate(Lease lease) {

        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<Lease>> violations = validator.validate(lease);
        if (violations.size() > 0) {
            LOGGER.log(Level.SEVERE, violations.toString());
            throw new InvalidLeaseException(INVALID_LEASE_MSG);
        }

        // End date cannot be before start date
        if (lease.endDate.before(lease.startDate)) {
            LOGGER.log(Level.SEVERE, INVALID_PERIOD_DATES_MSG);
            throw new InvalidLeaseException(INVALID_PERIOD_DATES_MSG);
        }

        // Start date cannot be after end date
        if (lease.startDate.after(lease.startDate)) {
            LOGGER.log(Level.SEVERE, INVALID_PERIOD_DATES_MSG);
            throw new InvalidLeaseException(INVALID_PERIOD_DATES_MSG);
        }

        // Finish date cannot be before start date
        if (lease.finishDate != null && lease.finishDate.before(lease.startDate)) {
            LOGGER.log(Level.SEVERE, INVALID_FINISHED_DATE_MSG);
            throw new InvalidLeaseException(INVALID_FINISHED_DATE_MSG);
        }

    }
}
