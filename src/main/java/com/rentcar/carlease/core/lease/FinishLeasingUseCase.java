package com.rentcar.carlease.core.lease;

import com.rentcar.carlease.core.lease.exception.LeaseNotFoundException;

import java.time.LocalDate;
import java.util.Date;

import static com.rentcar.carlease.core.lease.LeaseConstants.LEASE_NOT_FOUND_MSG;

public class FinishLeasingUseCase {

    private final LeaseRepository leaseRepository;

    public FinishLeasingUseCase(LeaseRepository leaseRepository) {
        this.leaseRepository = leaseRepository;
    }

    public void finishLeasing(String leaseId) {

        var opLease = leaseRepository.findById(leaseId);
        if (opLease.isEmpty()) {
            throw new LeaseNotFoundException(LEASE_NOT_FOUND_MSG);
        }

        Date finishDate = java.sql.Date.valueOf(LocalDate.now());

        Lease finishLease = new Lease(
                opLease.get().id,
                opLease.get().carId,
                opLease.get().driverId,
                opLease.get().startDate,
                opLease.get().endDate,
                finishDate
        );

        leaseRepository.finishLease(finishLease);
    }
}
