package com.rentcar.carlease.core.lease;

public final class LeaseConstants {

    private LeaseConstants() {}

    public static final String CAR_IN_ANOTHER_CONTRACT = "This car was already rented";

    public static final String INVALID_LEASE_MSG = "Invalid Driver id, car id, start date or end date";

    public static final String INVALID_PERIOD_DATES_MSG = "Invalid start or end date";

    public static final String INVALID_FINISHED_DATE_MSG = "Invalid finished date";

    public static final String LEASE_NOT_FOUND_MSG = "Leasing not found";

}
