package com.rentcar.carlease.core.lease;

import com.rentcar.carlease.core.car.CarRepository;
import com.rentcar.carlease.core.driver.Driver;
import com.rentcar.carlease.core.driver.exception.DriverNorFoundException;
import com.rentcar.carlease.core.driver.DriverService;
import com.rentcar.carlease.core.lease.exception.CarLeaseAlreadyRentedException;
import com.rentcar.carlease.core.lease.exception.CarLeaseNotFoundException;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import static com.rentcar.carlease.core.driver.DriverConstants.DRIVER_NOT_FOUND;
import static com.rentcar.carlease.core.lease.LeaseConstants.CAR_IN_ANOTHER_CONTRACT;

public class StartLeasingUseCase {

    private final LeaseRepository leaseRepository;

    // TODO: replace by a CarService when car-api is created
    private final CarRepository carRepository;

    private final LeaseValidator leaseValidator;

    private final DriverService driverService;

    public StartLeasingUseCase(
            LeaseRepository leaseRepository,
            DriverService driverService,
            LeaseValidator leaseValidator,
            CarRepository carRepository) {
        this.leaseRepository = leaseRepository;
        this.driverService = driverService;
        this.leaseValidator = leaseValidator;
        this.carRepository = carRepository;
    }

    public Lease startLeasing(Lease lease) {

        checkCar(lease);

        checkDriver(lease);

        leaseValidator.validate(lease);

        return leaseRepository.saveLease(lease);
    }

    private void checkCar(Lease lease) {

        var opCar = carRepository.findById(lease.carId);
        if (opCar.isEmpty()) {
            throw new CarLeaseNotFoundException(CAR_IN_ANOTHER_CONTRACT);
        }

        // A leasing contract can't be started if the car is in another active
        // contract
        List<Lease> currentLeases =
                leaseRepository.findByCarId(lease.carId).
                        stream().
                        filter( l -> l.finishDate == null).
                        collect(Collectors.toList());

        if (currentLeases.size() > 0) {
            throw new CarLeaseAlreadyRentedException(CAR_IN_ANOTHER_CONTRACT);
        }

    }

    private void checkDriver(Lease lease) {

        // The driver must exists in the driver API
        UUID driverId = UUID.fromString(lease.driverId);
        Optional<Driver> opDriver = driverService.getDriverById(driverId);
        if (opDriver.isEmpty()) {
            throw new DriverNorFoundException(DRIVER_NOT_FOUND);
        }

    }
}
