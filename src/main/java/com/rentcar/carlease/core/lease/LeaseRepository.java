package com.rentcar.carlease.core.lease;

import java.util.List;
import java.util.Optional;

public interface  LeaseRepository {

    List<Lease> findByDriverId(String driverId);

    List<Lease> findByCarId(String carId);

    Optional<Lease> findById(String leaseId);

    Lease saveLease(Lease lease);

    void finishLease(Lease lease);

}
