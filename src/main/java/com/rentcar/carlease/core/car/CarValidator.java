package com.rentcar.carlease.core.car;

import com.rentcar.carlease.core.car.exception.CarValidationException;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import static com.rentcar.carlease.core.car.CarConstants.CAR_INVALID_MSG;

public class CarValidator {

    private static final Logger LOGGER = Logger.getLogger(CarValidator.class.getName());

    public void validate(Car driver) {

        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<Car>> violations = validator.validate(driver);
        if (violations.size() > 0) {
            LOGGER.log(Level.SEVERE,CAR_INVALID_MSG);
            throw new CarValidationException(CAR_INVALID_MSG);
        }

    }
}
