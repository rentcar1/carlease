package com.rentcar.carlease.core.car;

import java.util.Optional;

public interface CarRepository {

    Car create(Car car);

    Optional<Car> findByLicense(String license);

    Optional<Car> findById(String id);

}
