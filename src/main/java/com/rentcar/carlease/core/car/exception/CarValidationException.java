package com.rentcar.carlease.core.car.exception;

public class CarValidationException extends RuntimeException{
    public CarValidationException(String message) {
        super(message);
    }
}
