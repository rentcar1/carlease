package com.rentcar.carlease.core.car;

import com.rentcar.carlease.core.car.exception.CarAlreadyExistsException;

public class CreateCarUseCase {

    private CarRepository carRepository;

    private CarValidator carValidator;

    public CreateCarUseCase(CarRepository carRepository, CarValidator carValidator) {
        this.carRepository = carRepository;
        this.carValidator = carValidator;
    }

    public Car createCar(Car car){

        var opCar = carRepository.findByLicense(car.license);
        if (opCar.isPresent()) {
            throw new CarAlreadyExistsException(CarConstants.CAR_ALREADY_EXISTS_MSG);
        }

        carValidator.validate(car);

        return carRepository.create(car);

    }
}
