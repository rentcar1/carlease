package com.rentcar.carlease.core.car;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class Car {

    public final String id;

    @NotNull(message = "Car license cannot be null")
    @NotEmpty(message = "Car license cannot be empty")
    @NotBlank(message = "Car license cannot be blank")
    public final String license;

    public Car(String id, String license) {
        this.id = id;
        this.license = license;
    }

    public static class Builder {
        private String id;
        private String license;

        public Car.Builder id(String id) {
            this.id = id;
            return this;
        }

        public Car.Builder license(String license) {
            this.license = license;
            return this;
        }

        public Car build(){
            return new Car(id, license);
        }
    }




}
