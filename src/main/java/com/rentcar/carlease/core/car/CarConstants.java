package com.rentcar.carlease.core.car;

public final class CarConstants {

    private CarConstants() {}

    public static final String DRIVER_NOT_FOUND_MSG = "Car not found";

    public static final String CAR_ALREADY_EXISTS_MSG = "Car license already exists";

    public static final String CAR_INVALID_MSG = "Car license is not valid";
}
