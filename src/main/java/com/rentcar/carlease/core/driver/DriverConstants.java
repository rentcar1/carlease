package com.rentcar.carlease.core.driver;

public final class DriverConstants {

    private DriverConstants() {}

    public static final String DRIVER_NOT_FOUND = "Driver not found";
}
