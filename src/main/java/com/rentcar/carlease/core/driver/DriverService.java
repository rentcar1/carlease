package com.rentcar.carlease.core.driver;

import java.util.Optional;
import java.util.UUID;

public interface DriverService {

    Optional<Driver>  getDriverById(UUID driverId);
}
