package com.rentcar.carlease.core.driver;

import java.util.UUID;

public class Driver {

    public final String id;

    public final String cpf;

    public Driver(String id, String cpf){
        this.id = id;
        this.cpf = cpf;
    }

    public static class Builder {
        private String id;
        private String cpf;

        public com.rentcar.carlease.core.driver.Driver.Builder id(String id) {
            this.id = id;
            return this;
        }

        public com.rentcar.carlease.core.driver.Driver.Builder cpf(String cpf) {
            this.cpf = cpf;
            return this;
        }

        public com.rentcar.carlease.core.driver.Driver build(){
            return new com.rentcar.carlease.core.driver.Driver(id, cpf);
        }
    }

}
