package com.rentcar.carlease.core.driver.exception;

public class DriverNorFoundException extends RuntimeException{
    public DriverNorFoundException(String message) {
        super(message);
    }
}
