# CarLease API
This project is a microservice for Carlease crud operations. The application architecture is based on [Clean Architeture](https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html) principles.
It uses the Spring Eureka as service discovery and Zuul as Gateway. 
Get the service discovey [here](https://gitlab.com/rentcar1/servicediscovery).
Get the gataway appplication [here](https://gitlab.com/rentcar1/gateway)

<p style="text-align: center;">
    <img src="arch.png" alt="Architeture" width="300px" heigth="300px">
</p>

## How it works?

* Core = The business logic, Use Cases and models
* Adapter = Integrates core with the application framework
* Framework = Any java web framework, in this case, Spring Boot 2.4.2

## Requirements
* java 11
* Maven
* Mongo Db

## Running the application
1. First create the database. A data.sql with the tables can be found at resources folder.
2. Create the environmental variables:
 *  SERVICE_DISCOVERY_URI - Example: http://localhost:8761/
 *  MONGODB_URI - point to mongo cluster
 *  MONGODB_DATABASE_NAME - point to a database
    
2. Start the Service Discovery application
3. Start the Getaway application
4. Start the Driver API application 
3. Run the command:
> mvn spring-boot:run

## Rest API Operations using Proxy
 
 ### Create a Car
 > POST localhost:8080/carlease-api/save
 
 Request:
 ```JSON
 {
 	"license":"XTB8976"
 }
 ```
 200 OK Response:
 ```JSON
 {
   "id": "600dcbc83429d1686c46c02c",
   "license": "XTB8976"
 }
 ```
 
 ### Start a Leasing
 > POST localhost:8080/carlease-api/leasing/start
 
 Request:
 ```JSON
  {
  	"carId":"600dd20888d7f416e1dade49",
  	"driverId":"c0a4dd2b-e835-4e98-8a58-e64b0a306de1",
  	"startDate":"2020-10-15",
  	"endDate":"2020-11-15"	
  }
  ```
 200 OK Response:
  ```JSON
 {
   "id": "600dd21488d7f416e1dade4a",
   "carId": "c0a4dd2b-e835-4e98-8a58-e64b0a306de1",
   "driverId": "600dd20888d7f416e1dade49",
   "startDate": "2020-10-15T00:00:00.000+00:00",
   "endDate": "2020-11-15T00:00:00.000+00:00",
   "finishDate": null
 }
  ```
 
 ### Finish a leasing
 > POST localhost:8080/carlease-api/leasing/finish
 
 Request:
  ```JSON
 {
 	"id":"600dd20888d7f416e1dade49"
 }
  ```
 200 OK Response - no body
  